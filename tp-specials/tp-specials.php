<?php
/*
Plugin Name: Travel Partners specials feedreader
Plugin URI: http://www.cdn.accommodationguru.com/wp-plugins/tp-specials/
Description: A widget to display Travel Partners specials feed on partner websites.
Version: 1.4.4
Author: Lafinboy
Author URI: http://www.lafinboy.com
*/

if (!defined('WP_CONTENT_URL')) {
    $tpspecials_url = get_option('siteurl') . '/wp-content/plugins/' . plugin_basename(dirname(__FILE__)).'/';
}else{
    $tpspecials_url = WP_CONTENT_URL . '/plugins/' . plugin_basename(dirname(__FILE__)) . '/';
}

if (!defined('TP_URL')) {
    define('TP_URL', $tpspecials_url);
}

if (!defined('TP_FEED_URL')) {
    DEFINE('TP_FEED_URL', 'http://www.travelpartners.com.au/feed/?post_type=tp_specials&tp_specials_region=');
}

if (!defined('TP_SVG')) {
    DEFINE('TP_SVG', '<svg width="0" height="0"><clipPath id="clipPolygon"><polygon points="0 85,155 105,180 0,10 13"></polygon></clipPath></svg>');
}

## Include the required styles
function tp_public_styles(){
    wp_register_style('tp-specials-css', TP_URL . 'css/tp-specials.css');
    wp_enqueue_style('tp-specials-css');

    wp_register_script( 'tp-specials-script', plugins_url( "js/tp-specials.js", __FILE__ ), array( "jquery" ) );
    wp_enqueue_script( 'tp-specials-script' );
}
add_action('wp_enqueue_scripts', 'tp_public_styles');

function tp_read_feed($email, $region, $tpshowstandout) {
    //test feed first
    $content = @file_get_contents(TP_FEED_URL . $region);
    try {
        $xml = new SimpleXmlElement($content);
    }
    catch (Exception $e) {
        /* the data provided is not valid XML */
        echo('<!-- DEBUG: Feed from ' . TP_FEED_URL . ' is not valid XML -->');
        exit();
    }

    if(!is_object($xml)){
        echo('<!-- DEBUG: Feed from ' . TP_FEED_URL . ' is not valid XML -->');
        return;
    }

    // Failover if we don't have the right feed content
    if($xml->channel->description != 'Travel Partner Specials') $xml = '';

    echo tp_parse_feed($xml, $email, $tpshowstandout);
}

function tp_parse_feed($xml, $email, $tpshowstandout) {
    // What to do if there is no XML
    if(empty($xml)) return '<!-- DEBUG: Feed from ' . TP_FEED_URL . ' returned no content -->';

    // Hash the email
    $hashed = tp_hash_var($email);
    $hashedURL = tp_hash_var(site_url());

    // Show the standout
    $hasstandout = false;
    $showstandout = true;
    $standoutCnt = 0;
    // Limit the standards
    $standardCnt = 0;
    $standardMax = 4; // count starts at 0

    $standout = '';
    $standard = '<div class="tpfeed--standard">';

    foreach($xml->channel->item as $item)
    {
        $featured = $item->featured;
        $link = $item->link . '?e=' . $hashed . '&r=' . $hashedURL;
        $imagepath = $item->imagepath;

        if ($featured == 'yes') {
            if ($tpshowstandout==='yes') {
                $hasstandout = true;
                $standout .= '<a href="'.$link.'" target="_blank" data-tpsimage="' . $imagepath . '" data-tpsitem="' . ++$standoutCnt . '">';
                // $standout .= '<div>';
                if((string) $item->standouttitle[0]!==""):
                    $standout .= '<h3>' . $item->standouttitle . '</h3>';
                endif;
                if((string) $item->standoutcontent[0]!==""):
                    $standout .= '<p>' . nl2br($item->standoutcontent) . '</p>';
                endif;
                $standout .= '</a>';
            }
        } else {
            if ($standardCnt===$standardMax) {
                continue;
            }
            $standard .= '<a href="'.$link.'" target="_blank" class="tpfeed--standard-wrapper">';
            $standard .= '<div  class="autoheight" style="background-image:url(' . $imagepath . ');">';
            $standard .= '<div class="callout"><span class="callout-line1">' . $item->callout1 . '</span>';
            $standard .= '<span class="callout-price">$' . $item->calloutprice .'<sup>*</sup> <span class="callout-unit">' . $item->calloutunit . '</span></span>';
            $standard .= '<span class="callout-line2">' . $item->callout2 . '</span>';
            $standard .= '</div></div>';
            $standard .= '<h3>' . $item->standardtitle;
            if((string) $item->subtitle!='') {
                $standard .= ' <span>' . $item->subtitle . '</span>';
            }
            $standard .= '</h3>';
            $standard .= '<p>' . $item->standardcontent . '</p>';
            $standard .= '</a>';
            $standardCnt++;
        }
    }

    // Set the standout class
    $standoutClass = '';
    if( $tpshowstandout==='no' || $hasstandout===false ){
        $showstandout = false;
        $standoutClass = ' nostandout';
    }

    $standout = $showstandout ? '<div class="tpfeed--standout"><i class="tpfeed--loader"></i>' . $standout . '<span class="tpfeed--prev"></span><span class="tpfeed--next"></span></div>' : $standout;

    $return = '<div class="tpfeed--container tpfeed--row">';//.$standoutClass.'">';

    $return .= $standout . $standard . '</div></div>';
    return $return . TP_SVG;
}

function tp_hash_var($var) {
    // simple, reversible hash of the var for use in the URL
    return base64_encode($var);
}

class TPSpecials extends WP_Widget {
    function __construct() {
        parent::__construct('tp_specials', __('Travel Partner Specials'), array('description' => __('Display the Travel Partner specials feed')));
    }

    function widget($args, $instance) {
        // widget options
        $tpemail = $instance['tpemail'];
        $tpregion = isset($instance['tpregion']) ? $instance['tpregion'] : "all";
        $tpshowstandout = isset($instance['tpshowstandout']) ? $instance['tpshowstandout'] : 'yes';

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];

        tp_read_feed($tpemail, $tpregion, $tpshowstandout, $tplayoutclass);

        echo $args['after_widget'];
    }

    /* saves options chosen from the widgets panel */
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['tpemail'] = strip_tags($new_instance['tpemail']);
        $instance['tpregion'] = strip_tags($new_instance['tpregion']);
        $instance['tpshowstandout'] = strip_tags($new_instance['tpshowstandout']);
        return $instance;
    }

    /* display widget in widgets panel */
    function form($instance) {
        $tpemail = esc_attr($instance['tpemail']);
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('tpemail'); ?>"><?php _e('Enquiry email address:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('tpemail'); ?>" name="<?php echo $this->get_field_name('tpemail'); ?>" type="text" value="<?php echo $tpemail; ?>" />
            <span>Enter the email address to which online enquiries will be sent.</span>
        </p>
        <?php
        $tpregion = esc_attr($instance['tpregion']);
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('tpregion'); ?>">Specials feed for (region):
                <select class='widefat' id="<?php echo $this->get_field_id('tpregion'); ?>"
                    name="<?php echo $this->get_field_name('tpregion'); ?>">
                    <option value='all'<?php echo ($tpregion=='all' || $tpregion == '')?' selected':''; ?>>
                        All
                    </option>
                    <option value='wa'<?php echo ($tpregion=='wa')?' selected':''; ?>>
                        West Coast
                    </option>
                </select>
            </label>
        </p>
        <?php
        $tpshowstandout = esc_attr($instance['tpshowstandout']);
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('tpshowstandout'); ?>">Show the standout specials:
                <select class='widefat' id="<?php echo $this->get_field_id('tpshowstandout'); ?>"
                    name="<?php echo $this->get_field_name('tpshowstandout'); ?>">
                    <option value='yes'<?php echo ($tpshowstandout=='yes' || $tpshowstandout == '')?' selected':''; ?>>
                        Yes
                    </option>
                    <option value='no'<?php echo ($tpshowstandout=='no')?' selected':''; ?>>
                        No
                    </option>
                </select>
            </label>
        </p><?php
    }
}

add_action( 'widgets_init', 'register_tpspecials');
function register_tpspecials() {
    register_widget( 'TPSpecials' );
}


/**
 * This is a custom handler tied to the Contact Form 7 plugin that decodes
 * an encoded part of the URL
 */
add_action( 'wpcf7_init', 'wpcf7_add_shortcode_getb64param' );

function wpcf7_add_shortcode_getb64param() {
    if ( function_exists( 'wpcf7_add_shortcode' ) ) {
        wpcf7_add_shortcode( 'getb64param', 'wpcf7_getb64param_shortcode_handler', true );
        wpcf7_add_shortcode( 'showb64param', 'wpcf7_showb64param_shortcode_handler', true );
    }
}

function wpcf7_getb64param_shortcode_handler($tag) {
    if (!is_array($tag)) return '';

    $name = $tag['name'];
    if (empty($name)) return '';

    $val = is_null($_GET[$name]) ? 'enquiry@travelpartners.com.au' : base64_decode($_GET[$name]);

    $html = '<input type="hidden" name="' . $name . '" value="'. $val . '" />';

    return $html;
}

function wpcf7_showb64param_shortcode_handler($tag) {
    if (!is_array($tag)) return '';

    $name = $tag['name'];
    if (empty($name)) return '';

    $html = base64_decode($_GET[$name]);
    return $html;
}

/**
 * Generic auto updater script for self-hosted plugins
 *
 * TODO: replace all instances of tp_specials to uniquely prefix funcs and vars
 */

$tp_specials_api_url = 'http://www.cdn.accommodationguru.com/wp-updates/api/';
$tp_specials_plugin_slug = basename(dirname(__FILE__));

// uncomment following line for testing
// set_site_transient('update_plugins', null);

// Take over the update check
add_filter('pre_set_site_transient_update_plugins', 'tp_specials_check_for_plugin_update');

function tp_specials_check_for_plugin_update($checked_data) {
    global $tp_specials_api_url, $tp_specials_plugin_slug, $wp_version;

    //Comment out these two lines during testing.
    if (empty($checked_data->checked)) {
        return $checked_data;
    }

    $args = array(
        'slug' => $tp_specials_plugin_slug,
        'version' => $checked_data->checked[$tp_specials_plugin_slug .'/'. $tp_specials_plugin_slug .'.php'],
    );
    $request_string = array(
            'body' => array(
                'action' => 'basic_check',
                'request' => serialize($args),
                'api-key' => md5(get_bloginfo('url'))
            ),
            'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
        );

    // Start checking for an update
    $raw_response = wp_remote_post($tp_specials_api_url, $request_string);

    if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
        $response = unserialize($raw_response['body']);

    if (is_object($response) && !empty($response)) // Feed the update data into WP updater
        $checked_data->response[$tp_specials_plugin_slug .'/'. $tp_specials_plugin_slug .'.php'] = $response;

    return $checked_data;
}


// Take over the Plugin info screen
add_filter('plugins_api', 'tp_specials_plugin_api_call', 10, 3);

function tp_specials_plugin_api_call($def, $action, $args) {
    global $tp_specials_plugin_slug, $tp_specials_api_url, $wp_version;

    if (!isset($args->slug) || ($args->slug != $tp_specials_plugin_slug))
        return false;

    // Get the current version
    $plugin_info = get_site_transient('update_plugins');
    $current_version = $plugin_info->checked[$tp_specials_plugin_slug .'/'. $tp_specials_plugin_slug .'.php'];
    $args->version = $current_version;

    $request_string = array(
            'body' => array(
                'action' => $action,
                'request' => serialize($args),
                'api-key' => md5(get_bloginfo('url'))
            ),
            'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
        );

    $request = wp_remote_post($tp_specials_api_url, $request_string);

    if (is_wp_error($request)) {
        $res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
    } else {
        $res = unserialize($request['body']);

        if ($res === false)
            $res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
    }

    return $res;
}

?>