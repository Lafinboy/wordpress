** Travel Partners specials feedreader **

A Wordpress widget created to request and parse for display an RSS XML feed of travel specials.

The travel specials are defined by the main site, and the feed can be displayed on any site.

The template has the option to display the feed as the carousel only, the specials only, or both as per example site http://www.travelpartners.com.au/

Each special is displayed with an affiliate link back to the details page on the TravelPartners website where a custom contact form collects the affiliate details.