jQuery( document ).ready( function($) {
    if ($('.tpfeed--standout') && $('.tpfeed--standout a').length > 1) {
        var tpFeedbox = $('.tpfeed--standout');
        var tpitems = $('a', tpFeedbox).length;
        var tpitemsLoaded = 1;
        var tpcurrent = 1;
        var tpdelay = 4500;
        var tpTimer;

        tpLoadImage();

        // stop when mouseover
        tpFeedbox.mouseenter(function() {
                clearInterval(tpTimer);
                tpFeedbox.removeClass('changing');
            })
            .mouseleave(function() {
                tpTimer = setInterval(tpslide, tpdelay);
            });

        // activate previous and next links
        $('.tpfeed--prev, .tpfeed--next').on('click', function() {
            if (tpFeedbox.hasClass('changing')) return;

            var lr = $(this).hasClass('tpfeed--next') ? 1:-1;
            tpslide(lr);
        });
    }

    function tpLoadImage() {
        if (tpitemsLoaded > tpitems) return;

        var $el = $('a[data-tpsitem=' + tpitemsLoaded + ']', tpFeedbox);
        var src = $el.data('tpsimage');
        var tptemp = new Image();

        tptemp.onload = function() {
            $el.css('background-image', 'url('+src+')').show();
            $('.tpfeed--loader').remove();

            if (tpTimer===undefined && tpitemsLoaded>1) {
               tpTimer = setInterval(tpslide, tpdelay);
               $('.tpfeed--prev, .tpfeed--next').show();
            }
            tpitemsLoaded++;
            tpLoadImage();
        }
        tptemp.src = src;

    }

    function tpslide(lr) {
        tpFeedbox.addClass('changing');

        if (lr===undefined) lr = 1;

        var n = tpcurrent+lr;
        var tpnext;
        if ( n > tpitems ) {
            tpnext = 1;
        } else if ( n < 1 ) {
            tpnext = tpitems;
        } else {
            tpnext = n;
        }

        var tpnextEl = $('a[data-tpsitem=' + tpnext + ']', tpFeedbox);

        tpnextEl.css('zIndex', 9).show();
        $('a[data-tpsitem=' + tpcurrent + ']', tpFeedbox)
            .fadeOut(800, 'linear', function(){
                $(this).css('zIndex', 1);
                tpnextEl.css('zIndex', 10);
                tpFeedbox.removeClass('changing');
            });

        tpcurrent = tpnext;
        if (tpcurrent>tpitems) {
            tpcurrent = 1;
        }
    }

    function tpslideImg(i) {
        if (1==='undefined') i = 1;

        return $('a[data-tpsitem=' + i + ']', tpFeedbox).data('tpsimage');
    }
});