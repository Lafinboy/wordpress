Run the following command in the /tp-specials plugin directory
to watch and autocompile the minified stylesheet into the /css directory

`sass --watch src/tp-specials.scss:css/tp-specials.css --style compressed`
