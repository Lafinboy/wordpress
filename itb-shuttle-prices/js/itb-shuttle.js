jQuery( document ).ready( function() {
    var itb_shuttleform_id = itbShuttleParams.gfid;
    var itb_shuttleprice_type = '';
    var itb_shuttleform_type = '';
    var itb_last_shuttleform_type = '';
    var pickupEl = jQuery('#input_'+itb_shuttleform_id+'_1');
    var dropoffEl = jQuery('#input_'+itb_shuttleform_id+'_20');

    // populate the dropoff field
    pickupEl.on('change', function(){
        // get the options for the dropoff field
        var idx = jQuery(this).prop('selectedIndex');

        itb_shuttleform_type = itb_shuttleprice_type = 'location';
        if (idx===0 || idx===3 || idx===4) {
            itb_shuttleform_type= '';
        }
        if (idx > 0 && idx < 3) {
            itb_shuttleform_type = itb_shuttleprice_type = 'suburb';
        }
        if (idx===3 || idx===4) {
            itb_shuttleprice_type = 'suburb';
        }

        console.log(idx, itb_shuttleform_type, itb_last_shuttleform_type);

        // do nothing if the type is the same as itb_last_shuttleform_type
        if (itb_shuttleform_type === itb_last_shuttleform_type) {
            // only get a new price
            jQuery('#input_'+itb_shuttleform_id+'_20').trigger('change');
            return;
        }

        var data = {
            action: 'shuttle_dropoffs_lookup',
            type: itb_shuttleform_type
        }
        jQuery.post(itbShuttleParams.ajaxurl, data, function(response){
            var locs = JSON.parse(response);
            jQuery('option:gt(0)', dropoffEl).remove();
            jQuery.each(locs, function(k,v) {
                dropoffEl.append(jQuery("<option>").attr("value",v).text(v));
            })
            dropoffEl.trigger('chosen:updated');
        });

        jQuery('#itb-shuttle--price').removeClass('loading').text('');

        itb_last_shuttleform_type = itb_shuttleform_type;
    });

    // Get price when the dropoff is selected
    dropoffEl.on('change', function(){
        console.log('doing dropoffEl');
        var p = pickupEl.val();
        var d = dropoffEl.val() || '';
        console.log(p,d,itb_shuttleprice_type);

        if (d!=='' && p!==''  && d!=='') {
            jQuery('#itb-shuttle--price').addClass('loading').text('Searching best fare . . .');
            var data = {
                action: 'shuttle_price_lookup',
                type: itb_shuttleprice_type,
                pickup: p,
                dropoff: d
            }
            jQuery.post(itbShuttleParams.ajaxurl, data, function(response){
                jQuery('#itb-shuttle--price').removeClass('loading').text('$' + response);
                jQuery('#input_'+itb_shuttleform_id+'_5').val('$' + response);
                jQuery('#gform_next_button_'+itb_shuttleform_id+'_7').css('display','initial');
            })
        }
    });

    // Get price when the dropoff is selected
    // jQuery('#input_'+itb_shuttleform_id+'_2,#input_'+itb_shuttleform_id+'_4').on('change', function(){
    //     console.log('doing jQuery change');
    //     itb_shuttleform_type = jQuery(this).closest('li').hasClass('itb-shuttle--suburbs') ? 'location':'suburb';
    //     var p = jQuery('#input_'+itb_shuttleform_id+'_1').val();
    //     var d = jQuery(this).val() || '';

    //     if (d!=='' && p!==''  && d!=='') {
    //         jQuery('#itb-shuttle--price').addClass('loading').text('Searching best fare . . .');
    //         var data = {
    //             action: 'shuttle_price_lookup',
    //             type: itb_shuttleform_type,
    //             pickup: p,
    //             dropoff: d
    //         }
    //         jQuery.post(itbShuttleParams.ajaxurl, data, function(response){
    //             jQuery('#itb-shuttle--price').removeClass('loading').text('$' + response);
    //             jQuery('#input_'+itb_shuttleform_id+'_5').val('$' + response);
    //             jQuery('#gform_next_button_'+itb_shuttleform_id+'_7').css('display','initial');
    //         })
    //     }
    // });
    // set datepicker fields to future only
    setTimeout(itbShuttleDatepickers, 1000);
    function itbShuttleDatepickers() {
        if(jQuery('#gform_'+itb_shuttleform_id).length > 0) {
            gform.addFilter( 'gform_datepicker_options_pre_init', function( optionsObj, formId, fieldId ) {
                if ( formId == itb_shuttleform_id && fieldId == 13 ) {
                    optionsObj.minDate = 0;
                    return optionsObj;
                }
            });
        }
    }
});