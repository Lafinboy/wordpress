## ITB Shuttle Prices ##

Generates a predefined form to enable shuttle bus bookings.

Prices are provided by the client in a CSV file and are imported into a database to enable quick fare calculations based on origin/destination selections.

Hooks into the Gravity Forms plugin for display and management fof the form.

Example can be seen at http://www.airporttransitlink.com.au/