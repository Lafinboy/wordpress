<?php
function itb_shuttleform_import() {
    global $wpdb;
    $gf_form = file_get_contents('shuttle-form.json', true);
    $itb_shuttleform_formdata = json_decode( $gf_form, true );

    $form_table_name = $wpdb->prefix . 'rg_form';
    $formTitle = $itb_shuttleform_formdata[0]['title'];

    // Test if form with $formTitle exists. If it does, update, otherwise add.
    if ( intval( $wpdb->get_var( $wpdb->prepare( "SELECT count(0) FROM {$form_table_name} WHERE title=%s", $formTitle ) ) ) === 0 ) {
        $result = GFAPI::add_form($itb_shuttleform_formdata[0]);
    } else {
        $formID = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM {$form_table_name} WHERE title=%s", $formTitle ) );
        $result = GFAPI::update_form($itb_shuttleform_formdata[0], $formID);
    }
    return $result;
}
