<?php
/*
Plugin Name: ITB Shuttle Prices
Plugin URI: http://www.cdn.accommodationguru.com/wp-plugins/itb-shuttle-prices/
Description: Shuttle prices.
Version: 1.1.1
Author: Lafinboy
Author URI: http://www.lafinboy.com
*/

define('ITBSP_DATABASE_VERSION', '1.1.1');
define('ITBSP_TABLE_NAME', 'itb_shuttle_prices');

add_action( 'wp_enqueue_scripts', 'itb_shuttle_init');
function itb_shuttle_init() {
    wp_enqueue_script( 'itb-shuttle-script', plugins_url( "js/itb-shuttle.js", __FILE__ ), array( "jquery" ), ITBSP_DATABASE_VERSION, true );
    wp_localize_script( 'itb-shuttle-script', 'itbShuttleParams', array(
            'gfid' => get_option('itb_shuttleform_id'),
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        ) );
}

add_action( 'wp_ajax_shuttle_dropoffs_lookup', 'shuttle_dropoffs_lookup_callback' );
add_action( 'wp_ajax_nopriv_shuttle_dropoffs_lookup', 'shuttle_dropoffs_lookup_callback' );
function shuttle_dropoffs_lookup_callback() {
    global $wpdb;

    $where = $_REQUEST['type']==='' ? '' : "where loc_type = '" . $_REQUEST['type'] . "'";
    $locs = $wpdb->get_col(
        "SELECT name FROM {$wpdb->prefix}itb_shuttle_prices {$where} ORDER BY id"
        );

    echo json_encode($locs);
    wp_die();
}

add_action( 'wp_ajax_shuttle_price_lookup', 'shuttle_price_lookup_callback' );
add_action( 'wp_ajax_nopriv_shuttle_price_lookup', 'shuttle_price_lookup_callback' );
function shuttle_price_lookup_callback() {
    global $wpdb;

    $suburb = $_REQUEST['pickup'];
    $surcharge = 0;
    if ($_REQUEST['type']==='suburb') {
        $suburb = $_REQUEST['dropoff'];
        // get the pickup surcharge
        $surcharge = $wpdb->get_var( $wpdb->prepare("
            SELECT surcharge_pickup
            FROM {$wpdb->prefix}itb_shuttle_prices
            WHERE name = '".$_REQUEST['pickup']."'
        ") );
    }

    $price_field = 'price_airport';
    if ($_REQUEST['pickup']==='MELBOURNE CBD' || $_REQUEST['dropoff']==='MELBOURNE CBD') {
        $price_field = 'price_cbd';
    }
    if ($_REQUEST['pickup']==='STATION PIER' || $_REQUEST['dropoff']==='STATION PIER') {
        $price_field = 'price_port';
    }
    if ($_REQUEST['pickup']==='MELBOURNE CBD' && $_REQUEST['dropoff']==='STATION PIER') {
        $price_field = 'price_cbd';
    }
    $price = $wpdb->get_var( $wpdb->prepare("
        SELECT {$price_field}
        FROM {$wpdb->prefix}itb_shuttle_prices
        WHERE name = %s",
        $suburb
    ) );

    // do some maths to add the price and surcharge together whilst maintaining float precision
    $s = $surcharge*100;
    $p = $price*100;
    $t = sprintf("%.2f", ($s+$p)/100.0);

    echo($t);

    wp_die();
}

register_activation_hook( __FILE__, 'itb_shuttle_activate' );
function itb_shuttle_activate() {
    global $wpdb;

    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . ITBSP_TABLE_NAME;

    // Drop the table first
    $wpdb->query("DROP TABLE $table_name");

    // Create new table
    $sql[] = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    loc_type varchar(255) NOT NULL,
    price_cbd decimal(10,2) NOT NULL,
    price_airport decimal(10,2) NOT NULL,
    price_port decimal(10,2) NOT NULL,
    surcharge_pickup decimal(10,2) NOT NULL,
    UNIQUE KEY id (id)
    ) $charset_collate;";
    // insert initial data
    $datasql = "INSERT INTO $table_name
        (name, loc_type, price_cbd, price_airport, price_port, surcharge_pickup)
        VALUES ";
    // read values data from CSV
    $fh = fopen(__DIR__ . "/prices_1-1.csv", "r");
    if ($fh) {
        while (($line = fgets($fh, 1000)) !== false) {
            $datasql .= "(" . $line . "),";
        }
        fclose($fh);
    }
    $sql[] = rtrim($datasql,',');

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );

    update_option('itb_shuttle_version', ITBSP_DATABASE_VERSION);

    // add the contact form
    require_once('shuttle-form-import.php');
    $gfID = itb_shuttleform_import();
    if(is_numeric($gfID)) {
        # set the new post id option
        update_option('itb_shuttleform_id', $gfID);
    }
}

register_deactivation_hook( __FILE__, 'itb_shuttle_deactivate' );
function itb_shuttle_deactivate() {
    // delete DB table and clear out options when deactivating
    global $wpdb;
    $table_name = $wpdb->prefix . ITBSP_TABLE_NAME;
    $sql = "DROP TABLE IF EXISTS $table_name";
    $wpdb->query($sql);

    // delete gf form and clear out options when deactivating
    GFAPI::delete_form(get_option('itb_shuttleform_id'));

    delete_option('itb_shuttleform_id');
    delete_option('itb_shuttle_version');

    do_action( 'itb_shuttle_deactivate' );
}

function itb_shuttle_update_db_check() {
    $version = get_option('itb_shuttle_version', '0.0.1');
    if ( version_compare($version, ITBSP_DATABASE_VERSION, '<' ) ) {
        itb_shuttle_activate();
    }
}
add_action( 'plugins_loaded', 'itb_shuttle_update_db_check' );

$formID = get_option('itb_shuttleform_id');

add_filter( 'gform_pre_render_' . $formID, 'populate_suburbs' );
add_filter( 'gform_pre_validation_' . $formID, 'populate_suburbs' );
add_filter( 'gform_pre_submission_filter_' . $formID, 'populate_suburbs' );
add_filter( 'gform_admin_pre_render_' . $formID, 'populate_suburbs' );
function populate_suburbs( $form ) {
    global $wpdb;

    $locs = $wpdb->get_results(
        "SELECT * FROM {$wpdb->prefix}itb_shuttle_prices ORDER BY id"
        );

    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-locations' ) === false ) {
            continue;
        }

        $choices = array();

        foreach ( $locs as $loc ) {
            $choices[] = array( 'text' => $loc->name, 'value' => $loc->name );
        }

        $field->placeholder = 'Select a location';
        $field->choices = $choices;

    }

    return $form;
}

add_filter( 'gform_ajax_spinner_url_' . $formID, 'change_spinner' );
function change_spinner( $src ) {
    return plugins_url( "images/spinner.gif", __FILE__ );
}

/**
 * Generic auto updater script for self-hosted plugins
 *
 * TODO: replace all instances of itb_shuttle_prices to uniquely prefix funcs and vars
 */

$itb_shuttle_prices_api_url = 'http://www.cdn.accommodationguru.com/wp-updates/api/';
$itb_shuttle_prices_plugin_slug = basename(dirname(__FILE__));

// uncomment following line for testing
// set_site_transient('update_plugins', null);

// Take over the update check
add_filter('pre_set_site_transient_update_plugins', 'itb_shuttle_prices_check_for_plugin_update');

function itb_shuttle_prices_check_for_plugin_update($checked_data) {
    global $itb_shuttle_prices_api_url, $itb_shuttle_prices_plugin_slug, $wp_version;

    //Comment out these two lines during testing.
    if (empty($checked_data->checked)) {
        return $checked_data;
    }

    $args = array(
        'slug' => $itb_shuttle_prices_plugin_slug,
        'version' => $checked_data->checked[$itb_shuttle_prices_plugin_slug .'/'. $itb_shuttle_prices_plugin_slug .'.php'],
        );
    $request_string = array(
        'body' => array(
            'action' => 'basic_check',
            'request' => serialize($args),
            'api-key' => md5(get_bloginfo('url'))
            ),
        'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
        );

    // Start checking for an update
    $raw_response = wp_remote_post($itb_shuttle_prices_api_url, $request_string);

    if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
        $response = unserialize($raw_response['body']);

    if (is_object($response) && !empty($response)) // Feed the update data into WP updater
    $checked_data->response[$itb_shuttle_prices_plugin_slug .'/'. $itb_shuttle_prices_plugin_slug .'.php'] = $response;

    return $checked_data;
}


// Take over the Plugin info screen
add_filter('plugins_api', 'itb_shuttle_prices_plugin_api_call', 10, 3);

function itb_shuttle_prices_plugin_api_call($def, $action, $args) {
    global $itb_shuttle_prices_plugin_slug, $itb_shuttle_prices_api_url, $wp_version;

    if (!isset($args->slug) || ($args->slug != $itb_shuttle_prices_plugin_slug))
        return false;

    // Get the current version
    $plugin_info = get_site_transient('update_plugins');
    $current_version = $plugin_info->checked[$itb_shuttle_prices_plugin_slug .'/'. $itb_shuttle_prices_plugin_slug .'.php'];
    $args->version = $current_version;

    $request_string = array(
        'body' => array(
            'action' => $action,
            'request' => serialize($args),
            'api-key' => md5(get_bloginfo('url'))
            ),
        'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
        );

    $request = wp_remote_post($itb_shuttle_prices_api_url, $request_string);

    if (is_wp_error($request)) {
        $res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
    } else {
        $res = unserialize($request['body']);

        if ($res === false)
            $res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
    }

    return $res;
}