## ITB Packages ##

Creates a Wordpress Custom Post Type to enable entry of travel package details, and display of those details in various formats on the public facing pages of the site via shortcodes.

The custom post type hooks into the Advanced Custom Fields plugin to generate the content fields for the custom post type, and on the public pages into the Gravity Forms plugin for the contact form.

Example of use can be seen at http://www.demo78.hostguys.biz/cruises/