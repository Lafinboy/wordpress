<?php
function itbp_cf7_import( $xml_string, $allow_duplicates = false, $update_if_exists = false ) {
    // Parse CF7 post's XML
    $content = simplexml_load_string( $xml_string, 'SimpleXMLElement', LIBXML_NOCDATA);
    // @TODO: add a check on $content
    // Parse XML post attributes containing fields
    $wp_post_attributes = $content->channel->item->children('wp', true);
    # Copy basic properties from the exported field
    $wp_post_data = array(
        'post_type'   => 'wpcf7_contact_form',
        'post_title'  => $content->channel->item->title,
        'post_name'   => $wp_post_attributes->post_name,
        'post_status' => 'publish',
        'post_author' => 1
        );
    $the_post = get_page_by_title( $content->channel->item->title, 'OBJECT', 'wpcf7_contact_form' );
    # Create a new post if doesn't exist
    if ( !$the_post || $allow_duplicates == true ) {
        $post_id = wp_insert_post( $wp_post_data );
    }
    # If exists, update post_meta (the actual editable fields created )
    else {
        $post_id = $the_post->ID;
    }
    # set the new post id option
    update_option('itbp_cf7_id', $post_id);
    # Prevents overwriting if post already exists
    if( $update_if_exists === true ) {
        $wp_post_meta = $content->channel->item->children( 'wp', true );
        if( $wp_post_meta ) {
            foreach ( $wp_post_meta as $row ) {
                // Choose only arrays (postmeta)
                if( count($row) > 0) {
                    $k = (string) $row->meta_key;
                    $v = maybe_unserialize( (string) $row->meta_value );
                    update_post_meta( $post_id, $k, $v );
                }
            }
            return true;
        }
    }
    return false;
}
?>