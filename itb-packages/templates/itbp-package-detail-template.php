<?php

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}
?>
<?php
add_filter('body_class', 'itbp_detail_body_classes');
function itbp_detail_body_classes($classes) {
    $classes[] = 'single-itb-packages';
    return $classes;
}
?>
<?php get_header(); ?>

<div id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
    <?php while( have_posts() ): the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
            <div class="post-content">
            <?php
            // Test if this package has expired
            date_default_timezone_set('Australia/Sydney');
            $ed = get_field('itbp_expiry_date');
            if ( $ed && strtotime('today') > strtotime($ed) ) { ?>
                <h1>Package Expired</h1>
                <p>Sorry, this package is no longer available</p>
            <?php
            } else {
                $packageTitle = get_field('itbp_short_title');
                if (get_field('itbp_origin')) {
                    $packageTitle .= ' - From ' . get_field('itbp_origin');
                }
                if (get_field('itbp_destination')) {
                    $packageTitle .= ' To ' . get_field('itbp_destination');
                }
                if (get_field('itbp_duration')) {
                    $packageTitle .= ' - ' . get_field('itbp_duration') . ' Nights';
                }
            ?>
                <h1><?php echo $packageTitle; ?></h1>

                <?php $i = get_field('itbp_package_image');?>
                <?php $qr = get_field('itbp_show_quick_enquiry');
                $qrClass = $qr ? ' package-qr ' : ' ';
                ?>
                <div class="package-header<?php echo $qrClass; ?>fusion-clearfix">
                    <div class="fusion-one-third fusion-layout-column">
                        <?php echo wp_get_attachment_image( $i['id'], 'full' ); ?>
                    </div>
                    <div class="fusion-two-fifth fusion-layout-column">
                        <dl>
                            <?php if (get_field('itbp_origin')) : ?>
                            <dt>Departing from:</dt>
                            <dd><?php the_field('itbp_origin'); ?></dd>
                            <?php endif;
                            if (get_field('itbp_destination') && get_field('itbp_origin') !== get_field('itbp_destination')) : ?>
                            <dt>Destination:</dt>
                            <dd><?php the_field('itbp_destination'); ?></dd>
                            <?php endif; ?>
                            <?php if (get_field('itbp_duration')): ?>
                            <dt>Duration:</dt>
                            <dd><?php the_field('itbp_duration'); ?> Nights</dd>
                            <?php endif;
                            if (get_field('itbp_offer')): ?>
                            <dt>Offer:</dt>
                            <dd><?php the_field('itbp_offer'); ?></dd>
                            <?php endif;
                            if (get_field('itbp_show_travel_dates')) : ?>
                            <dt>Travel Dates:</dt>
                            <dd><?php echo date('d/m/Y', strtotime(get_field('itbp_travel_date_from'))); ?> to <?php echo date('d/m/Y', strtotime(get_field('itbp_travel_date_to'))); ?></dd>
                                <?php if (get_field('itbp_show_travel_dates_2')) : ?>
                                <dd><?php echo date('d/m/Y', strtotime(get_field('itbp_travel_date_from_2'))); ?> to <?php echo date('d/m/Y', strtotime(get_field('itbp_travel_date_to_2'))); ?></dd>
                                <?php endif; ?>
                                <?php if (get_field('itbp_show_travel_dates_3')) : ?>
                                <dd><?php echo date('d/m/Y', strtotime(get_field('itbp_travel_date_from_3'))); ?> to <?php echo date('d/m/Y', strtotime(get_field('itbp_travel_date_to_3'))); ?></dd>
                                <?php endif; ?>
                                <?php if (get_field('itbp_show_travel_dates_4')) : ?>
                                <dd><?php echo date('d/m/Y', strtotime(get_field('itbp_travel_date_from_4'))); ?> to <?php echo date('d/m/Y', strtotime(get_field('itbp_travel_date_to_4'))); ?></dd>
                                <?php endif; ?>
                                <?php if (get_field('itbp_show_travel_dates_5')) : ?>
                                <dd><?php echo date('d/m/Y', strtotime(get_field('itbp_travel_date_from_5'))); ?> to <?php echo date('d/m/Y', strtotime(get_field('itbp_travel_date_to_5'))); ?></dd>
                                <?php endif; ?>
                            <?php endif;
                            $operators = wp_get_post_terms(get_the_ID(), 'itb_packages_operator', array("fields" => "names"));
                                if ($operators):
                            ?>
                            <dt>Tour Operator:</dt>
                            <dd><?php echo join($operators, ','); ?>
                            <?php endif ?>
                            <?php if (get_field('itbp_reference_code')): ?>
                            <dt>Reference Code:</dt>
                            <dd><?php the_field('itbp_reference_code') ?></dd>
                            <?php endif; ?>
                        </dl>
                    </div>
                    <div class="fusion-one-fourth fusion-layout-column fusion-column-last">
                        <?php if(get_field('itbp_price') && get_field('itbp_price_from')) : ?>
                        <p class="itbp-pricefrom">From</p>
                        <?php endif;
                        if (get_field('itbp_price')) : ?>
                        <?php $itbpCurrency = get_field('itbp_currency') === false ? 'AUD$' : get_field('itbp_currency'); ?>
                        <p class="itbp-price"><span><?php echo $itbpCurrency; ?></span><?php echo number_format(get_field('itbp_price')); ?></p>
                        <p class="itbp-priceper">Price per person</p>
                        <?php endif;
                        if (get_field('itbp_saving')): ?>
                        <p class="itbp-saving">Save <?php if(get_field('itbp_price_from')){ ?>up to <?php } ?><span>$</span><?php echo number_format(get_field('itbp_saving')); ?></p>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if ($qr): ?>
                <div class="package-qr--container">
                <?php /* are there images to show */
                    $qrImg1 = get_field('itbp_panel_image_1');
                    $qrImg2 = get_field('itbp_panel_image_2');
                    $qrImg3 = get_field('itbp_panel_image_3');
                    $qrImg4 = get_field('itbp_panel_image_4');

                    if ($qrImg1 || $qrImg2 || $qrImg3 || $qrImg4):
                ?>
                    <div class="flexslider">
                        <ul class="slides">
                        <?php if ($qrImg1): ?>
                            <li>
                                <?php echo wp_get_attachment_image($qrImg1['id'], 'full' ); ?>
                            </li>
                        <?php endif; ?>
                        <?php if ($qrImg2): ?>
                            <li>
                                <?php echo wp_get_attachment_image($qrImg2['id'], 'full' ); ?>
                            </li>
                        <?php endif; ?>
                        <?php if ($qrImg3): ?>
                            <li>
                                <?php echo wp_get_attachment_image($qrImg3['id'], 'full' ); ?>
                            </li>
                        <?php endif; ?>
                        <?php if ($qrImg4): ?>
                            <li>
                                <?php echo wp_get_attachment_image($qrImg4['id'], 'full' ); ?>
                            </li>
                        <?php endif; ?>
                        </ul>
                    </div>
                <?php endif; ?>
                    <div class="package-qr--form">
                        <p>This deal won't last long!</p>
                        <?php // add the Gravity Form
                        gravity_form( get_option('itbp_gfqr_id'), false, false, true, array(
                            'packageName' => $packageTitle,
                            'packageURL' => get_site_url(null, '/package/' . get_query_var("package") . '/')
                            ), true, 99);
                        ?>
                    </div>
                </div>
                <?php endif; ?>

                <h2 class="clearleft">Package Details</h2>
                <?php if (get_field('itbp_overview')) : ?>
                <h3>Overview</h3>
                <p><?php the_field('itbp_overview'); ?></p>
                <?php endif;
                if (get_field('itbp_details')): ?>
                    <h3>Details</h3>
                    <?php the_field('itbp_details'); ?>
                <?php endif;
                if (get_field('itbp_inclusions')) :?>
                <h3>Inclusions</h3>
                <?php the_field('itbp_inclusions'); ?>
                <?php endif; ?>
                <h2 class="clearfix">Travel Enquiry</h2>
                <?php // add the Gravity Form
                gravity_form( get_option('itbp_gf_id'), false, false, true, array(
                    'packageFrom' => get_field('itbp_origin'),
                    'packageTo' => get_field('itbp_destination'),
                    'packageName' => $packageTitle,
                    'packageURL' => get_site_url(null, '/package/' . get_query_var("package") . '/')
                    ), true);
                ?>
            <?php } ?>
            </div>
        </article>
    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
</div>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */