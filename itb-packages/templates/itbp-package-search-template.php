<?php

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}
?>
<?php
add_filter('body_class', 'test_body_classes');
function test_body_classes($classes) {
    $classes[] = 'itb-package-search';
    return $classes;
}
?>
<?php get_header(); ?>

<div id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
    <?php //while( have_posts() ): the_post(); ?>
        <article <?php post_class( 'post' ); ?>>
            <div class="post-content">
                <?php // build up the shortcode parameters
                // get the destination id if a package_destination is defined
                $scTerms = array('All Travel Styles', 'All Operators', 'Worldwide', 'All Codes');
                $scParams = '';
                // get the type id if a package_type is defined
                if ($_POST['packageType'] != '') {
                    $scParams .= ' package_type="'. $_POST['packageType'] .'"';
                    $ptName = get_term_by('slug',$_POST['packageType'],'itb_packages_type');
                    $scTerms[0] = $ptName->name;
                }
                // get the operator id if a package_operator is defined
                if ($_POST['packageOperator'] != '') {
                    $scParams .= ' package_operator="'. $_POST['packageOperator'] .'"';
                    $poName = get_term_by('slug',$_POST['packageOperator'],'itb_packages_operator');
                    $scTerms[1] = $poName->name;
                }
                if ($_POST['packageDestination'] != '') {
                    $scParams .= ' package_destination="'. $_POST['packageDestination'] .'"';
                    $destName = get_term_by('slug',$_POST['packageDestination'],'itb_packages_destination');
                    $scTerms[2] = $destName->name;
                }
                if ($_POST['packageCode'] != '') {
                    $scParams .= ' package_code="'. $_POST['packageCode'] .'"';
                    $scTerms[3] = 'Code: ' . $_POST['packageCode'];
                }
                ?>
                <h1>Tour Search Results</h1>
                <?php if (count($scTerms)>0) : ?>
                <p>Search terms: <?php echo join(' - ', $scTerms); ?></p>
                <?php endif; ?>
                <?php echo do_shortcode('[itb_packages'. $scParams .']') ?>
            </div>
        </article>
    <?php //endwhile; ?>
    <?php // wp_reset_query(); ?>
</div>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */