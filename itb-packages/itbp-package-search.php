<?php
// Enable Package Search Page
add_action( 'init', array('ITB_Package_Search','itbp_add_rewrite_rules' ));
class ITB_Package_Search {
  public static function itbp_add_rewrite_rules() {
    add_rewrite_rule( '^packagesearch/([^/]+)/?', 'index.php?packagesearch=$matches[1]', 'top' );
    add_filter('query_vars', array(__CLASS__, 'itbp_query_vars'), 1, 1);
    add_filter('template_redirect', array(__CLASS__, 'itbp_template_redirect'), 1, 0);

    global $wp_rewrite;
    $wp_rewrite->flush_rules(false);
  }

  public static function itbp_query_vars($vars) {
    array_push($vars, 'packagesearch');
    return $vars;
  }

  public static function itbp_template_redirect() {
    global $wp_query;

    if($wp_query->query_vars["name"] == 'packagesearch') {
      $wp_query->is_404 = false;
      header("HTTP/1.1 200 OK");

      add_filter( 'template_include', array(__CLASS__, 'itbp_template_path'));
    }
  }

  public static function itbp_template_path() {
    return plugin_dir_path( __FILE__ ) . '/templates/itbp-package-search-template.php';
  }
}
