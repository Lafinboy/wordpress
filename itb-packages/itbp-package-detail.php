<?php
// Enable Package Detail Pages
add_action( 'init', array('ITB_Package_Detail','itbp_add_rewrite_rules' ));
class ITB_Package_Detail {
  public static function itbp_add_rewrite_rules() {
    add_rewrite_rule( '^package/([^/]*)/?', 'index.php?package=$matches[1]', 'top' );
    add_filter('query_vars', array(__CLASS__, 'itbp_query_vars'), 1, 1);
    add_filter('template_redirect', array(__CLASS__, 'itbp_template_redirect'), 1, 0);

    global $wp_rewrite;
    $wp_rewrite->flush_rules(false);
  }

  public static function itbp_query_vars($vars) {
    array_push($vars, 'package');
    return $vars;
  }

  public static function itbp_template_redirect() {
    global $wp_query;

    if(isset($wp_query->query_vars["package"])) {
      $post = get_page_by_path( $wp_query->query_vars["package"], OBJECT, 'itb_packages' );

      $wp_query->is_404 = false; // maybe not needed
      $wp_query->is_feed = false; // maybe not needed
      $wp_query->queried_object = $post;
      $wp_query->post = new WP_Post($post);
      $wp_query->found_posts = 1;
      $wp_query->post_count = 1;
      $wp_query->max_num_pages = 1;
      $wp_query->is_single = 1;
      $wp_query->is_404 = false;
      $wp_query->is_home = false;
      $wp_query->posts = array($post);
      $wp_query->page = true;

      add_filter( 'template_include', array(__CLASS__, 'itbp_template_path'));
    }
  }

  public static function itbp_template_path() {
    return plugin_dir_path( __FILE__ ) . '/templates/itbp-package-detail-template.php';
  }
}

add_action( 'wpcf7_init', 'wpcf7_add_shortcode_getitbpparam' );

function wpcf7_add_shortcode_getitbpparam() {
    if ( function_exists( 'wpcf7_add_shortcode' ) ) {
        wpcf7_add_shortcode( 'getitbpparam', 'wpcf7_getitbpparam_shortcode_handler', true );
        wpcf7_add_shortcode( 'showitbpparam', 'wpcf7_showitbpparam_shortcode_handler', true );
    }
}

function wpcf7_getitbpparam_shortcode_handler($tag) {
    if (!is_array($tag)) return '';

    $name = $tag['name'];
    if (empty($name)) return '';

    $html = '<input type="hidden" name="' . $name . '" value="'. esc_attr( $GLOBALS[$name] ) . '" />';
    return $html;
}

function wpcf7_showitbpparam_shortcode_handler($tag) {
    if (!is_array($tag)) return '';

    $name = $tag['name'];
    if (empty($name)) return '';

    $html = esc_html( $GLOBALS[$name] );
    return $html;
}