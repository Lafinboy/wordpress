<?php
function itbp_acf_create_field( $xml_string, $allow_duplicates = false, $update_if_exists = false ) {
    // Parse ACF post's XML
    $content = simplexml_load_string( $xml_string, 'SimpleXMLElement', LIBXML_NOCDATA);
    // @TODO: add a check on $content
    // Parse XML post attributes containing fields
    $wp_post_attributes = $content->channel->item->children('wp', true);
    # Copy basic properties from the exported field
    $wp_post_data = array(
        'post_type'   => 'acf',
        'post_title'  => $content->channel->item->title,
        'post_name'   => $wp_post_attributes->post_name,
        'post_status' => 'publish',
        'post_author' => 1
        );
    $the_post = get_page_by_title( $content->channel->item->title, 'OBJECT', 'acf' );
    # Create a new post if doesn't exist
    if ( !$the_post || $allow_duplicates == true ) {
        $post_id = wp_insert_post( $wp_post_data );
    }
    # If exists, update post_meta (the actual editable fields created )
    else {
        $post_id = $the_post->ID;
    }
    # Prevents overwriting if post already exists
    if( $update_if_exists === true ) {
        $wp_post_meta = $content->channel->item->children( 'wp', true );
        if( $wp_post_meta ) {
            foreach ( $wp_post_meta as $row ) {
                // Choose only arrays (postmeta)
                if( count($row) > 0) {
                    // using addlashes on meta values to compensate for stripslashes() that will be run upon import
                    update_post_meta( $post_id, (string)$row->meta_key, addslashes( (string)$row->meta_value ) );
                }
            }
            return true;
        }
    }
    return false;
}
?>