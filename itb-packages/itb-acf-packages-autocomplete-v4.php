<?php

class itb_acf_packages_autocomplete extends acf_field {

	// vars
	var $settings, // will hold info such as dir / path
		$defaults; // will hold default field options


	function __construct()
	{

		// vars
		$this->name = 'itb-acf-packages-autocomplete';
		$this->label = __('Package Locations Autocomplete');
		$this->category = __("Basic",'acf'); // Basic, Content, Choice, etc
		$this->defaults = array();

		// do not delete!
    	parent::__construct();

 		add_action( 'wp_ajax_package_autocomplete_ajax', array( $this, 'autocomplete_packages_ajax_callback' ) );

	}


	public function autocomplete_packages_ajax_callback() {
		global $wpdb;

		$results = array();

		$results = $wpdb->get_col( $wpdb->prepare( "
	        SELECT DISTINCT meta_value FROM {$wpdb->postmeta}
	        WHERE (meta_key LIKE %s
	        OR meta_key LIKE %s)
	        AND meta_value LIKE %s
	        ORDER BY meta_value
			", 'itbp_origin', 'itbp_destination', $_REQUEST['request'].'%' ) );

		echo json_encode($results);

		wp_die();

	}


	function create_field( $field )
	{

		?>
		<input type="text" name="<?php echo esc_attr($field['name']) ?>" value="<?php echo esc_attr($field['value']) ?>" />
		<?php

	}


	function input_admin_enqueue_scripts()
	{

		$dir = plugin_dir_url( __FILE__ );

		wp_register_script( 'itb-acf-packages-input-autocomplete', "{$dir}js/input.js", array( "jquery-ui-autocomplete" ) );
		wp_enqueue_script('itb-acf-packages-input-autocomplete');

		wp_register_style( 'itb-acf-packages-input-autocomplete', "{$dir}css/input.css", array('acf-input') );
		wp_enqueue_style('itb-acf-packages-input-autocomplete');

	}


	function update_value( $value, $post_id, $field )
	{
		// Trim to remove duplicity
		return trim($value);
	}

}


// create field
new itb_acf_packages_autocomplete();

?>
