<?php
function itbp_gf_import($datafile) {
    global $wpdb;
    $gf_form = file_get_contents($datafile, true);
    $itbp_gf_formdata = json_decode( $gf_form, true );

    $form_table_name = $wpdb->prefix . 'rg_form';
    $formTitle = $itbp_gf_formdata[0]['title'];

    // Test if form with $formTitle exists. If it does, update, otherwise add.
    if ( intval( $wpdb->get_var( $wpdb->prepare( "SELECT count(0) FROM {$form_table_name} WHERE title=%s", $formTitle ) ) ) === 0 ) {
        $result = GFAPI::add_form($itbp_gf_formdata[0]);
    } else {
        $formID = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM {$form_table_name} WHERE title=%d", $formTitle ) );
        $result = GFAPI::update_form($itbp_gf_formdata[0], $formID);
    }
    return $result;
}
