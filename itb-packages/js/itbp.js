jQuery( document ).ready( function() {
    // give the button time to be added to the DOM
    setTimeout(itbp_fickle, 1000);

    function itbp_fickle() {
        jQuery( '#itbp-dismiss-cf7-email > .notice-dismiss' ).on('click', function() {
            itbpDismissCF7Email( jQuery( '#itbp-dismiss-cf7-email' ).data( 'nonce' ) );
        });
    }
})

function itbpDismissCF7Email( nonce ) {
    jQuery.post( ajaxurl, {
            action: 'itbp-dismiss-cf7-email',
            _wpnonce: nonce
        }
    );
}