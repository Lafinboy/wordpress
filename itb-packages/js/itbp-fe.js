(function($){
    $( document ).ready( function() {
        if ($('.single-itb_packages').length>0) {
        // depart and destination fields with those values and disable them
            $('li.gf_readonly input').attr('readonly','readonly');
        var gfid = itbPackageParams.gfid;
        // set datepicker fields to future only
        gform.addFilter( 'gform_datepicker_options_pre_init', function( optionsObj, formId, fieldId ) {
            if ( formId == gfid && fieldId == 19 ) {
                optionsObj.minDate = 1;
                optionsObj.onClose = function (dateText, inst) {
                    // get the return date and only change it if it's less than departure date
                        var r = $('#input_'+gfid+'_20').datepicker('getDate');
                    var l = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
                    var d = Math.ceil((r - l)/1000/60/60/24);

                    if(d<1) {
                        dateMin = new Date(inst.selectedYear, inst.selectedMonth, parseInt(inst.selectedDay, 10) + 1);
                            $('#input_'+gfid+'_20').datepicker('option', 'minDate', dateMin).datepicker('setDate', dateMin);
                    }
                };
            }
            if ( formId == gfid && fieldId == 20 ) {
                optionsObj.minDate = 2;
            }
            return optionsObj;
        });
    }
        // For the search form
        $('.itb-package-search-form').each(function (index){
            fEl = $(this);

            // Layout
            if (fEl.parents('.menu-item-type-yawp_wim').length>0) {
                var o = fEl.offset();
                var l = (0 - o.left) + 'px';
                var w = $(window).width() + 'px';

                fEl.closest('.menu-item-type-yawp_wim').css({
                    'left': l,
                    'width': w
});

                fEl.closest('.sub-menu').addClass('itb-packages-nobg');
            }

            // Functionality
            $('.itbp-select input', fEl).on('click', function(){
                $(this).next('.itbp_destinations').toggle();
            });
            $('select, input:not([readonly])', fEl).on('click', function() {
                $('.itbp_destinations, .itbp-sub-menu', fEl).hide();
            });

            $('.itbp_destinations a', fEl).on('click', function(a) {
                a.preventDefault();
                $(this).closest('.itbp-select').find('input').val($(this).text());
                $('.itbp_destinations, .itbp-sub-menu').hide();
            })

            $('.itbp-open-submenu', fEl).on('click', function(el) {
                $('.itbp-sub-menu', fEl).hide();

                var e = $(this).next('ul');

                if (e.is(':visible')) {
                    e.hide();
                } else {
                    e.show();
                }
            })
        })
    });
})(jQuery);