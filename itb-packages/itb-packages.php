<?php
/*
Plugin Name: ITB Packages
Plugin URI: http://www.cdn.accommodationguru.com/wp-plugins/itb-packages/
Description: Custom page type for displaying Packages.
Version: 2.6.6
Author: Lafinboy
Author URI: http://www.lafinboy.com
*/

require_once('itbp-package-detail.php');
require_once('itbp-package-search.php');

add_action( 'admin_init', 'itbp_reqd_plugin_check' );
function itbp_reqd_plugin_check() {
    // We need to have Gravity Forms plugin installed for this all to work.
    if ( is_admin() ) {
        if ( current_user_can( 'activate_plugins' ) &&  (!is_plugin_active('gravityforms/gravityforms.php') || !is_plugin_active('gravity-fieldset-for-gravity-forms/gravity-fieldset-for-gravity-forms.php')) ) {
            add_action( 'admin_notices', 'itbp_child_plugin_notice' );
        }
    }
}
function itbp_child_plugin_notice(){
    $class = "error";
    $message = "ERROR: ITB Packages plugin requires both <a href='http://www.gravityforms.com/' target='_blank'>Gravity Forms</a> and <a href='https://wordpress.org/plugins/gravity-fieldset-for-gravity-forms/' target='_blak'>Gravity Fieldset for Gravity Forms</a> plugins to be installed and active.";
    echo "<div class=\"$class\"><p>$message</p></div>";
}

add_action( 'init', 'itbp_cpt_init');
function itbp_cpt_init() {
    // Package Post Types
    register_post_type(
        'itb_packages',
        array(
            'labels' => array(
                'name' => 'Packages',
                'singular_name' => 'Package',
                'menu_name' => 'Packages',
                'add_new' => 'Add New Package',
                'add_new_item' => 'Add New Package',
                'edit_item' => 'Edit Package',
                'new_item' => 'New Package',
                'search_items' => 'Search Packages',
                'not_found' => 'No Packages found',
                'not_found_in_trash' => 'No Packages found in Trash'
                ),
            'menu_position' => 20,
            'capability_type' => 'page',
            'public' => true,
            'has_archive' => false,
            'hierarchical' => false,
            'rewrite' => array('slug' => 'package'),
            'can_export' => true,
            'menu_icon' => 'dashicons-palmtree'
            )
        );

    // Add new taxonomy, make it hierarchical (like categories)
    $catLabels = array(
        'name'              => _x( 'Package Categories', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search Categories', 'textdomain' ),
        'all_items'         => __( 'All Categories', 'textdomain' ),
        'parent_item'       => __( 'Parent Category', 'textdomain' ),
        'parent_item_colon' => __( 'Parent Category:', 'textdomain' ),
        'edit_item'         => __( 'Edit Category', 'textdomain' ),
        'update_item'       => __( 'Update Category', 'textdomain' ),
        'add_new_item'      => __( 'Add New Category', 'textdomain' ),
        'new_item_name'     => __( 'New Category Name', 'textdomain' ),
        'menu_name'         => __( 'Package Category', 'textdomain' ),
        );

    $catArgs = array(
        'hierarchical'      => true,
        'labels'            => $catLabels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'package-categories' ),
        );
    register_taxonomy('itb_packages_category', 'itb_packages', $catArgs);

    // Add new taxonomy, make it hierarchical (like categories)
    $destLabels = array(
        'name'              => _x( 'Package Destinations', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'Destination', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search Destinations', 'textdomain' ),
        'all_items'         => __( 'All Destinations', 'textdomain' ),
        'parent_item'       => __( 'Parent Destination', 'textdomain' ),
        'parent_item_colon' => __( 'Parent Destination:', 'textdomain' ),
        'edit_item'         => __( 'Edit Destination', 'textdomain' ),
        'update_item'       => __( 'Update Destination', 'textdomain' ),
        'add_new_item'      => __( 'Add New Destination', 'textdomain' ),
        'new_item_name'     => __( 'New Destination Name', 'textdomain' ),
        'menu_name'         => __( 'Package Destination', 'textdomain' ),
        );

    $destArgs = array(
        'hierarchical'      => true,
        'labels'            => $destLabels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'package-destinations' ),
        );
    register_taxonomy('itb_packages_destination', 'itb_packages', $destArgs);

    // Add new taxonomy, NOT hierarchical (like tags)
    $stopLabels = array(
        'name'                       => _x( 'Package Stops', 'taxonomy general name', 'textdomain' ),
        'singular_name'              => _x( 'Type', 'taxonomy singular name', 'textdomain' ),
        'search_items'               => __( 'Search Stops', 'textdomain' ),
        'popular_items'              => __( 'Popular Stops', 'textdomain' ),
        'all_items'                  => __( 'All Stops', 'textdomain' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit Type', 'textdomain' ),
        'update_item'                => __( 'Update Type', 'textdomain' ),
        'add_new_item'               => __( 'Add New Type', 'textdomain' ),
        'new_item_name'              => __( 'New Type Name', 'textdomain' ),
        'separate_items_with_commas' => __( 'Separate stops with commas', 'textdomain' ),
        'add_or_remove_items'        => __( 'Add or remove stops', 'textdomain' ),
        'choose_from_most_used'      => __( 'Choose from the most used stops', 'textdomain' ),
        'not_found'                  => __( 'No Stops found.', 'textdomain' ),
        'menu_name'                  => __( 'Package Stops', 'textdomain' ),
        );

    $stopArgs = array(
        'hierarchical'          => false,
        'labels'                => $stopLabels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'package-stops' ),
        );
    register_taxonomy('itb_packages_stop', 'itb_packages', $stopArgs);

    // Add new taxonomy, NOT hierarchical (like tags)
    $typeLabels = array(
        'name'                       => _x( 'Package Types', 'taxonomy general name', 'textdomain' ),
        'singular_name'              => _x( 'Type', 'taxonomy singular name', 'textdomain' ),
        'search_items'               => __( 'Search Types', 'textdomain' ),
        'popular_items'              => __( 'Popular Types', 'textdomain' ),
        'all_items'                  => __( 'All Types', 'textdomain' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit Type', 'textdomain' ),
        'update_item'                => __( 'Update Type', 'textdomain' ),
        'add_new_item'               => __( 'Add New Type', 'textdomain' ),
        'new_item_name'              => __( 'New Type Name', 'textdomain' ),
        'separate_items_with_commas' => __( 'Separate types with commas', 'textdomain' ),
        'add_or_remove_items'        => __( 'Add or remove types', 'textdomain' ),
        'choose_from_most_used'      => __( 'Choose from the most used types', 'textdomain' ),
        'not_found'                  => __( 'No Types found.', 'textdomain' ),
        'menu_name'                  => __( 'Package Types', 'textdomain' ),
        );

    $typeArgs = array(
        'hierarchical'          => false,
        'labels'                => $typeLabels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'package-types' ),
        );
    register_taxonomy('itb_packages_type', 'itb_packages', $typeArgs);

    // Add new taxonomy, NOT hierarchical (like tags)
    $opLabels = array(
        'name'                       => _x( 'Package Operators', 'taxonomy general name', 'textdomain' ),
        'singular_name'              => _x( 'Operator', 'taxonomy singular name', 'textdomain' ),
        'search_items'               => __( 'Search Operators', 'textdomain' ),
        'popular_items'              => __( 'Popular Operators', 'textdomain' ),
        'all_items'                  => __( 'All Operators', 'textdomain' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit Operator', 'textdomain' ),
        'update_item'                => __( 'Update Operator', 'textdomain' ),
        'add_new_item'               => __( 'Add New Operator', 'textdomain' ),
        'new_item_name'              => __( 'New Operator Name', 'textdomain' ),
        'separate_items_with_commas' => __( 'Separate operators with commas', 'textdomain' ),
        'add_or_remove_items'        => __( 'Add or remove operators', 'textdomain' ),
        'choose_from_most_used'      => __( 'Choose from the most used operators', 'textdomain' ),
        'not_found'                  => __( 'No Operators found.', 'textdomain' ),
        'menu_name'                  => __( 'Package Operators', 'textdomain' ),
        );

    $opArgs = array(
        'hierarchical'          => false,
        'labels'                => $opLabels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'package-operators' ),
        );
    register_taxonomy('itb_packages_operator', 'itb_packages', $opArgs);

    function itbp_wordwrap($in, $limit = 165) {
        $out = explode("\n", wordwrap($in, $limit, "\n"));
        return $out[0] . "&hellip;";
    }
}

add_action( 'wp_enqueue_scripts', 'itbp_cpt_enqueue');
function itbp_cpt_enqueue() {
    wp_enqueue_style( 'itbp-css', plugins_url( 'css/packages.css', __FILE__ ) );
    wp_enqueue_style( 'itbp-detailscss', plugins_url( 'css/packages-details.css', __FILE__ ) );
    wp_enqueue_style( 'itbp-searchcss', plugins_url( 'css/package-search.css', __FILE__ ) );

    wp_register_script( 'itbp-fe-script', plugins_url( "js/itbp-fe.js", __FILE__ ), array( "jquery" ) );
    wp_enqueue_script( 'itbp-fe-script' );
    wp_localize_script( 'itbp-fe-script', 'itbPackageParams', array(
        'gfid' => get_option('itbp_gf_id')
        ) );
}

function itbp_cpt_helpscreen(){

    //get the current screen object
    $current_screen = get_current_screen();

    //show only on book listing page
    if($current_screen->post_type == 'itb_packages' && $current_screen->base == 'edit'){
        $contentA = '';
        $contentA .= '<p>Individual packages can be created and assigned to a single or multiple Package Categories, Package Destinations, Package Type, and/or Package Operators</p><p>Place the following shortcode on a page to display a list of active packages:</p><p><code>[itb_packages]</code></p><p>Additional filtering of the package list can be obtained by defining the any combination of the following options:</p><ul><li>Package Category: <code>[itb_packages package_category="categoryslug"]</code></li><li>Package Destination: <code>[itb_packages package_destination="destinationslug"]</code></li><li>Package Type: <code>[itb_packages package_type="typeslug"]</code></li><li>Package Operator: <code>[itb_packages package_operator="operatorslug"]</code></li></ul><p>The value of each filter option is the `slug` name for that option.</p><hr><p><strong>Sort Order</strong></p><p>The package list is displayed sorted by the number of nights for each package, in ascending order. Alternative sort orders are:</p><ul><li>Published Date: <code>[itb_packages sort_by="pubdate"] (newest to oldest)</code></li><li>Package Price: <code>[itb_packages sort_by="price"] (cheapest to most expensive)</code></li></ul><hr><p><strong>Fixed Limit</strong></p><p>By default, all packages matching the selection criteria are displayed. A limited number of packages can be displayed by setting a limit. For example:</p><ul><li>Limit to 3: <code>[itb_packages limit="3"]</code></li><li>Limit to 10: <code>[itb_packages limit="10"]</code></li></ul>';
        $current_screen->add_help_tab( array(
            'id'        => 'itbp_display_pacakges_tab',
            'title'     => __('Display Packages'),
            'content'   => $contentA
            )
        );
        $contentB = '';
        $contentB .= '<p>The generic package search widget can be displayed by using the following shortcode:</p><p><code>[itb_packages_search]</code></p>';
        $current_screen->add_help_tab( array(
            'id'        => 'itbp_packages_search_tab',
            'title'     => __('Search Packages'),
            'content'   => $contentB
            )
        );
    }
}
add_action('admin_head', 'itbp_cpt_helpscreen');

function itbp_activate() {
    require_once('acf_import.php');
    require_once('gf_import.php');
    // add the custom fields
    $acf_fields = file_get_contents('acf-packages-export.xml', true);
    itbp_acf_create_field($acf_fields, false, true);
    // add the contact forms
    $gfID = itbp_gf_import('gf-form.json');
    if(is_numeric($gfID)) {
        # set the new post id option
        update_option('itbp_gf_id', $gfID);
    }
    $gfqrID = itbp_gf_import('gfqr-form.json');
    if(is_numeric($gfqrID)) {
        # set the new post id option
        update_option('itbp_gfqr_id', $gfqrID);
    }
}

function itbp_cpt_rewrite_flush() {
    // First, we "add" the custom post type via the above written function.
    // Note: "add" is written with quotes, as CPTs don't get added to the DB,
    // They are only referenced in the post_type column with a post entry,
    // when you add a post of this CPT.
    itbp_activate();

    // ATTENTION: This is *only* done during plugin activation hook in this example!
    // You should *NEVER EVER* do this on every page load!!
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'itbp_cpt_rewrite_flush' );

function itbp_cpt_deactivate() {
    // delete gf form and clear out options when deactivating
    GFAPI::delete_form(get_option('itbp_gf_id'));
    delete_option('itbp_gf_id');
    GFAPI::delete_form(get_option('itbp_gfqr_id'));
    delete_option('itbp_gfqr_id');

    do_action( 'itbp_cpt_deactivate' );
}
register_deactivation_hook( __FILE__, 'itbp_cpt_deactivate' );

// force functions to run when updates are complete to pull in any new custom field data
add_action( 'upgrader_process_complete', 'itbp_cpt_rewrite_flush', 10, 2 );

// Load ACF4
function register_fields_itbp_autocomplete() {
    include_once('itb-acf-packages-autocomplete-v4.php');
}

add_action('acf/register_fields', 'register_fields_itbp_autocomplete');

/**
 * Enable display of lists via shortcode
 */
add_shortcode('itb_packages', 'itb_packages_shortcode_fn');

function itb_packages_shortcode_fn($atts, $content = null) {
    $atts = shortcode_atts(array(
        'package_category'    => '',
        'package_destination' => '',
        'package_type' => '',
        'package_operator' => '',
        'package_code' => '',
        'sort_by' => 'nights',
        'limit' => -1
        ), $atts);

    $queryArgs = array(
        'post_status'         => 'publish',
        'post_type'           => 'itb_packages'
        );

    // define the sort method and order
    switch ($atts['sort_by']) {
        case 'pubdate':
            $queryArgs['orderby'] = 'date';
            $queryArgs['order'] = 'DESC';
            break;
        case 'price':
            $queryArgs['meta_key'] = 'itbp_price';
            $queryArgs['meta_type'] = 'DECIMAL';
            $queryArgs['orderby'] = 'meta_value_num';
            $queryArgs['order'] = 'ASC';
            break;
        case 'nights':
        default:
            $queryArgs['meta_key'] = 'itbp_duration';
            $queryArgs['orderby'] = 'meta_value_num';
            $queryArgs['order'] = 'ASC';
            break;
    }

    // set the limit
    $queryArgs['posts_per_page'] = intval($atts['limit'], 10);

    if ($atts['package_category'] != '' | $atts['package_destination'] != '' | $atts['package_type'] != '' | $atts['package_operator'] != '' | $atts['package_code'] != '') {
        $queryArgs['tax_query']['relation'] = 'AND';
    }
    // get the category id if a package_category is defined
    if ($atts['package_category'] != '') {
        $queryArgs['tax_query'][] = array(
            'taxonomy' => 'itb_packages_category',
            'field' => 'slug',
            'terms' =>  array($atts['package_category'])
            );
    }
    // get the destination id if a package_destination is defined
    if ($atts['package_destination'] != '') {
        $queryArgs['tax_query'][] = array(
            'taxonomy' => 'itb_packages_destination',
            'field' => 'slug',
            'terms' =>  $atts['package_destination']
            );
    }
    // get the type id if a package_type is defined
    if ($atts['package_type'] != '') {
        $queryArgs['tax_query'][] = array(
            'taxonomy' => 'itb_packages_type',
            'field' => 'slug',
            'terms' =>  $atts['package_type']
            );
    }
    // get the operator id if a package_operator is defined
    if ($atts['package_operator'] != '') {
        $queryArgs['tax_query'][] = array(
            'taxonomy' => 'itb_packages_operator',
            'field' => 'slug',
            'terms' =>  $atts['package_operator']
            );
    }
    // get the operator id if a package_operator is defined
    if ($atts['package_code'] != '') {
        $queryArgs['meta_query'] = array (
            array (
                'key' => 'itbp_reference_code',
                'value' => $atts['package_code'],
                'compare' => 'IN',
                ),
            );
    }

    $args = array(
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<div class="widget-title"><h3>',
        'after_title'   => '</h3></div>',
        );

    $itbp_query = new WP_Query($queryArgs);

    $out = '';

    if ($itbp_query->have_posts()) {
        $out .= '<div id="itbp-container">';
        $itbp_row = -1;

        while ( $itbp_query->have_posts() ) : $itbp_query->the_post();
        // check if this item is expired
        date_default_timezone_set('Australia/Sydney');
        $ed = get_field('itbp_expiry_date');
        if ( $ed && strtotime('today') > strtotime($ed) ) {
            $itbp_row++;
            continue;
        }

        $itbpPackageImage = get_field('itbp_package_image');
        $itbpShortTitle = get_field('itbp_short_title');
        $itbpOrigin = get_field('itbp_origin');
        $itbpDestination = get_field('itbp_destination');
        $itbpDuration = get_field('itbp_duration');
        $itbpShowDates = get_field('itbp_show_travel_dates');
        $itbpTravelDateFrom = date('d/m/y', strtotime(get_field('itbp_travel_date_from')));
        $itbpTravelDateTo = date('d/m/y', strtotime(get_field('itbp_travel_date_to')));
        $itbpShowDates2 = get_field('itbp_show_travel_dates_2');
        $itbpTravelDateFrom2 = date('d/m/y', strtotime(get_field('itbp_travel_date_from_2')));
        $itbpTravelDateTo2 = date('d/m/y', strtotime(get_field('itbp_travel_date_to_2')));
        $itbpShowDates3 = get_field('itbp_show_travel_dates_3');
        $itbpTravelDateFrom3 = date('d/m/y', strtotime(get_field('itbp_travel_date_from_3')));
        $itbpTravelDateTo3 = date('d/m/y', strtotime(get_field('itbp_travel_date_to_3')));
        $itbpShowDates4 = get_field('itbp_show_travel_dates_4');
        $itbpTravelDateFrom4 = date('d/m/y', strtotime(get_field('itbp_travel_date_from_4')));
        $itbpTravelDateTo4 = date('d/m/y', strtotime(get_field('itbp_travel_date_to_4')));
        $itbpShowDates5 = get_field('itbp_show_travel_dates_5');
        $itbpTravelDateFrom5 = date('d/m/y', strtotime(get_field('itbp_travel_date_from_5')));
        $itbpTravelDateTo5 = date('d/m/y', strtotime(get_field('itbp_travel_date_to_5')));
        $itbpCurrency = get_field('itbp_currency') === false ? 'AUD$' : get_field('itbp_currency');
        $itbpPrice = get_field('itbp_price');
        $itbpPriceFrom = get_field('itbp_price_from');
        $itbpOffer = get_field('itbp_offer');
        $itbpOverview = get_field('itbp_overview');
        $itbpInclusions = get_field('itbp_inclusions');

        $itbpFromTo = $itbpTravelDateFrom. ' - ' . $itbpTravelDateTo;
        $itbpFromTo2 = $itbpTravelDateFrom2. ' - ' . $itbpTravelDateTo2;
        $itbpFromTo3 = $itbpTravelDateFrom3. ' - ' . $itbpTravelDateTo3;
        $itbpFromTo4 = $itbpTravelDateFrom4. ' - ' . $itbpTravelDateTo4;
        $itbpFromTo5 = $itbpTravelDateFrom5. ' - ' . $itbpTravelDateTo5;

        switch(get_field('itbp_howshow')) {
            case 'page':
            $itbpLink = get_field('itbp_pagelink');
            break;
            case 'url':
            $itbpLink = get_field('itbp_linktourl');
            break;
            case 'iframe':
                $itbpLink = '/ext/' . base64_encode(get_field('itbp_framelink')) . '/itbpid/' . get_the_ID();
                break;
            default:
            $itbpLink = get_permalink();
        }

        $out .= '<section class="itbp-row"><div class="itbp-imagebox">';
        if ($itbpPackageImage) {
            $out .= '<img src="' . $itbpPackageImage['url'] . '" alt="">';
        }
        $out .= '</div><div class="itbp-mainbox"><h4>' . $itbpShortTitle . '</h4>';
        $out .= '<p class="itbp-mainbox--byeline">';
        if ($itbpDuration) {
            $out .= $itbpDuration . ' Nights';
        }
        if ($itbpDuration && $itbpOffer) {
            $out .= ' - ';
        }
        if ($itbpOffer) {
            $out .= $itbpOffer;
        }
        $out .= '</p>';
        if ($itbpOverview) {
            $out .= '<p><span class="itbp-label">Overview:</span> ' . itbp_wordwrap($itbpOverview) . '</p>';
        }
        if ($itbpShowDates) {
            $out .= '<p><span class="itbp-label">Travel Dates:</span> ' . $itbpFromTo;
            if ($itbpShowDates2) {
                $out .= ', ' . $itbpFromTo2;
            }
            if ($itbpShowDates3) {
                $out .= ', ' . $itbpFromTo3;
            }
            if ($itbpShowDates4) {
                $out .= ', ' . $itbpFromTo4;
            }
            if ($itbpShowDates5) {
                $out .= ', ' . $itbpFromTo5;
            }

            $out .= '</p>';
        }
        $out .= '</div><div class="itbp-pricebox">';
        if ($itbpPrice && $itbpPriceFrom) {
            $out .= '<p class="itbp-pricefrom">From</p>';
        }
        if ($itbpPrice) {
            $out .= '<p class="itbp-price"><span>' . $itbpCurrency . '</span>' . number_format($itbpPrice) . '</p>';
            $out .= '<p class="itbp-priceper">Price per person</p>';
        }
        $out .= '<a href="' . $itbpLink . '">View Details</a></div>';
        $out .= '</section>';
        endwhile;

        $out .= '</div>';
    } else {
        $out = '<p>No results found for your search terms.</p>';
    }

    return $out;
}

/**
 * Enable package callout panel via shortcode
 */
add_shortcode('itb_packages_callout', 'itb_packages_callout_shortcode_fn');

function itb_packages_callout_shortcode_fn($atts, $content = null) {
    $atts = shortcode_atts(array(
        'itbpid'    => ''
        ), $atts);

    $out = '';

    if ($atts['itbpid']!=='' && get_field('itbp_showcallout', $atts['itbpid'])) {
        $out = '<div class="itbp-callout"><div>';
        $out .= '<a href="' . get_field('itbp_calloutlink', $atts['itbpid']);
        $out .= '" target="_blank"><span>';
        $out .= get_field('itbp_callouttext', $atts['itbpid']);
        $out .= '</span></a></div></div>';
    }

    return $out;
}

/**
 * Enable generic search via shortcode
 */
add_shortcode('itb_packages_search', 'itb_packages_search_shortcode_fn');

function itb_packages_search_shortcode_fn($atts, $content = null) {

    $out = '';

    $typeTerms = get_terms('itb_packages_type', array('hide_empty' => false));
    $operatorTerms = get_terms('itb_packages_operator', array('hide_empty' => false));
    $destTerms = get_terms('itb_packages_destination', array('hide_empty' => false));

    $out = '<div class="itb-package-search-form"><form action="/packagesearch/" method="post">';
    $out .= '<select name="packageType"><option value="">All Travel Styles</option>' . get_terms_hierarchical_select($typeTerms) . '</select>';
    $out .= '<select name="packageOperator"><option value="">All Operators</option>' . get_terms_hierarchical_select($operatorTerms) . '</select>';
    $out .= '<div class="itbp-select"><input name="packageDestination" value="" placeholder="Worldwide" readonly />';
    $out .= '<ul class="itbp_destinations">' . get_terms_hierarchical_list($destTerms);
    $out .= '</div>';
    $out .= '<input name="packageCode" value="" placeholder="Tour codes" class="itbp-codes" />';
    $out .= '<input type="submit"></form></div>';

    return $out;
}

function get_terms_hierarchical_list($terms, $output = '', $parent_id = 0, $level = 0) {
    $hasOpener = false;
    $hasCloser = false;

    foreach ($terms as $term) {
        if ($parent_id == $term->parent) {
            if (!$hasOpener && $level>0) {
                $output .= '<span class="itbp-open-submenu"></span>';
                $output .= '<ul class="itbp-sub-menu opener">';
                $hasOpener = true;
            }
            $hasCloser = true;
            $output .= '<li><a href="#">' . $term->name . '</a>';
            $output = get_terms_hierarchical_list($terms, $output, $term->term_id, $level + 1);
        }
    }
    if($hasCloser) {
        $output .= '</li></ul>';
    }
    return $output;
}

function get_terms_hierarchical_select($terms, $output = '', $parent_id = 0, $level = 0) {
    //Out Template
    $outputTemplate = '<option value="%SLUG%">%PADDING%%NAME%</option>';

    foreach ($terms as $term) {
        if ($parent_id == $term->parent) {
            //Replacing the template variables
            $itemOutput = str_replace('%SLUG%', $term->slug, $outputTemplate);
            $itemOutput = str_replace('%PADDING%', str_pad('', $level*12, '&nbsp;&nbsp;&nbsp;&nbsp;'), $itemOutput);
            $itemOutput = str_replace('%NAME%', $term->name, $itemOutput);

            $output .= $itemOutput;
            $output = get_terms_hierarchical_select($terms, $output, $term->term_id, $level + 1);
        }
    }
    return $output;
}

/**
 * Recursively sort an array of taxonomy terms hierarchically. Child categories will be
 * placed under a 'children' member of their parent term.
 * @param Array   $cats     taxonomy term objects to sort
 * @param Array   $into     result array to put them in
 * @param integer $parentId the current parent ID to put them in
 */
function sort_terms_hierarchicaly(Array &$cats, Array &$into, $parentId = 0)
{
    foreach ($cats as $i => $cat) {
        if ($cat->parent == $parentId) {
            $into[$cat->term_id] = $cat;
            unset($cats[$i]);
        }
    }

    foreach ($into as $topCat) {
        $topCat->children = array();
        sort_terms_hierarchicaly($cats, $topCat->children, $topCat->term_id);
    }
}

/**
 * Generic auto updater script for self-hosted plugins
 *
 * TODO: replace all instances of itb_packages to uniquely prefix funcs and vars
 */

$itb_packages_api_url = 'http://www.cdn.accommodationguru.com/wp-updates/api/';
$itb_packages_plugin_slug = basename(dirname(__FILE__));

// uncomment following line for testing
// set_site_transient('update_plugins', null);

// Take over the update check
add_filter('pre_set_site_transient_update_plugins', 'itb_packages_check_for_plugin_update');

function itb_packages_check_for_plugin_update($checked_data) {
    global $itb_packages_api_url, $itb_packages_plugin_slug, $wp_version;

    //Comment out these two lines during testing.
    if (empty($checked_data->checked)) {
        return $checked_data;
    }

    $args = array(
        'slug' => $itb_packages_plugin_slug,
        'version' => $checked_data->checked[$itb_packages_plugin_slug .'/'. $itb_packages_plugin_slug .'.php'],
        );
    $request_string = array(
        'body' => array(
            'action' => 'basic_check',
            'request' => serialize($args),
            'api-key' => md5(get_bloginfo('url'))
            ),
        'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
        );

    // Start checking for an update
    $raw_response = wp_remote_post($itb_packages_api_url, $request_string);

    if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
        $response = unserialize($raw_response['body']);

    if (is_object($response) && !empty($response)) // Feed the update data into WP updater
    $checked_data->response[$itb_packages_plugin_slug .'/'. $itb_packages_plugin_slug .'.php'] = $response;

    return $checked_data;
}


// Take over the Plugin info screen
add_filter('plugins_api', 'itb_packages_plugin_api_call', 10, 3);

function itb_packages_plugin_api_call($def, $action, $args) {
    global $itb_packages_plugin_slug, $itb_packages_api_url, $wp_version;

    if (!isset($args->slug) || ($args->slug != $itb_packages_plugin_slug))
        return false;

    // Get the current version
    $plugin_info = get_site_transient('update_plugins');
    $current_version = $plugin_info->checked[$itb_packages_plugin_slug .'/'. $itb_packages_plugin_slug .'.php'];
    $args->version = $current_version;

    $request_string = array(
        'body' => array(
            'action' => $action,
            'request' => serialize($args),
            'api-key' => md5(get_bloginfo('url'))
            ),
        'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
        );

    $request = wp_remote_post($itb_packages_api_url, $request_string);

    if (is_wp_error($request)) {
        $res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
    } else {
        $res = unserialize($request['body']);

        if ($res === false)
            $res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
    }

    return $res;
}
?>